import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import reducedfractionapp.ReducedFraction;

public class multiplyReducedFractionTest {
    @Test
    void multiplyAllPositiveTest() {

        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeLeftNumTest() {
        ReducedFraction frac1 = new ReducedFraction(-15,9);
        ReducedFraction frac2 = new ReducedFraction(9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(-1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeLeftDenomTest() {
        ReducedFraction frac1 = new ReducedFraction(15,-9);
        ReducedFraction frac2 = new ReducedFraction(9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(-1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeRightNumTest() {
        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(-9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(-1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeRightDenomTest() {
        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(9,-15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(-1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeLeftNumRightDenomTest() {
        ReducedFraction frac1 = new ReducedFraction(-15,9);
        ReducedFraction frac2 = new ReducedFraction(9,-15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyPartiallyNegativeRightNumLeftDenomTest() {
        ReducedFraction frac1 = new ReducedFraction(15,-9);
        ReducedFraction frac2 = new ReducedFraction(-9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(1, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyLeftNumZeroTest() {
        ReducedFraction frac1 = new ReducedFraction(0,9);
        ReducedFraction frac2 = new ReducedFraction(9,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(0, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyRightNumZeroTest() {
        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(0,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(0, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }

    @Test
    void multiplyLeftRightNumZeroTest() {
        ReducedFraction frac1 = new ReducedFraction(0,9);
        ReducedFraction frac2 = new ReducedFraction(0,15);

        frac1 = frac1.multiply(frac2);

        Assert.assertEquals(0, frac1.getNumerator());
        Assert.assertEquals(1, frac1.getDenominator());
    }
}
