import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import reducedfractionapp.ReducedFraction;

public class reducedFractionCreationOnlyNumTest {
    @Test()
    void olyNumPositiveCreationTest() {
        ReducedFraction frac = new ReducedFraction(15);

        Assert.assertEquals(15, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }

    @Test
    void onlyNumNegativeCreationTest() {
        ReducedFraction frac = new ReducedFraction(-15);

        Assert.assertEquals(-15, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }


    @Test
    void zeroNumCreationTest() {
        ReducedFraction frac = new ReducedFraction(0);

        Assert.assertEquals(0, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }
}
