import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import reducedfractionapp.ReducedFraction;

public class equalsReducedFractionTest {
    @Test
    void areEqualsTest() {

        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(5,3);

        boolean exp_res = true;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsAllPositiveTest() {

        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(6,3);

        boolean exp_res = false;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsLeftNumNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(-15,9);
        ReducedFraction frac2 = new ReducedFraction(15,9);

        boolean exp_res = false;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsRightNumNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(-15,9);

        boolean exp_res = false;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsLeftDenomNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(15,-9);
        ReducedFraction frac2 = new ReducedFraction(15,9);

        boolean exp_res = false;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsRightDenomNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(15,9);
        ReducedFraction frac2 = new ReducedFraction(15,-9);

        boolean exp_res = false;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsLeftNumRightDenomNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(-15,9);
        ReducedFraction frac2 = new ReducedFraction(15,-9);

        boolean exp_res = true;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void areNotEqualsRightNumLeftDenomNegativeTest() {

        ReducedFraction frac1 = new ReducedFraction(15,-9);
        ReducedFraction frac2 = new ReducedFraction(-15,9);

        boolean exp_res = true;
        boolean res = frac1.equals(frac2);

        Assert.assertEquals(exp_res, res);
    }
}
