import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import reducedfractionapp.ReducedFraction;

public class reducedFractionCreationTest {
    @Test()
    void notReducedCreationTest() {
        ReducedFraction frac = new ReducedFraction(15,9);

        Assert.assertEquals(5, frac.getNumerator());
        Assert.assertEquals(3, frac.getDenominator());
    }

    @Test
    void partiallyNegativeCreationNumTest() {
        ReducedFraction frac = new ReducedFraction(-15,9);

        Assert.assertEquals(-5, frac.getNumerator());
        Assert.assertEquals(3, frac.getDenominator());
    }

    @Test
    void partiallyNegativeCreationDenomTest() {
        ReducedFraction frac = new ReducedFraction(15,-9);

        Assert.assertEquals(-5, frac.getNumerator());
        Assert.assertEquals(3, frac.getDenominator());
    }

    @Test
    void fullyNegativeCreationTest() {
        ReducedFraction frac = new ReducedFraction(-15,-9);

        Assert.assertEquals(5, frac.getNumerator());
        Assert.assertEquals(3, frac.getDenominator());
    }

    @Test
    void almostZeroNumCreationTest() {
        ReducedFraction frac = new ReducedFraction(0,1);

        Assert.assertEquals(0, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }

    @Test
    void almostZeroDenomCreationTest() {
        ReducedFraction frac = new ReducedFraction(15,0);

        Assert.assertEquals(0, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }

    @Test
    void zeroCreationTest() {
        ReducedFraction frac = new ReducedFraction(0,0);

        Assert.assertEquals(0, frac.getNumerator());
        Assert.assertEquals(1, frac.getDenominator());
    }
}
