import junit.framework.Assert;
import org.junit.jupiter.api.Test;
import reducedfractionapp.ReducedFraction;

public class toDoubleReducedFractionTest {
    @Test
    void fullyPositiveTest() {
        ReducedFraction frac = new ReducedFraction(15,9);

        double exp_res = (double)15/(double)9;
        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void partiallyPositiveDenomTest() {
        ReducedFraction frac = new ReducedFraction(-15,9);

        double exp_res = (double)15/(double)9;
        exp_res *= -1;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void partiallyPositiveNumTest() {
        ReducedFraction frac = new ReducedFraction(15,-9);

        double exp_res = (double)15/(double)9;
        exp_res *= -1;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void fullyNegativeTest() {
        ReducedFraction frac = new ReducedFraction(-15,-9);

        double exp_res = (double)15/(double)9;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void zeroNumTest() {
        ReducedFraction frac = new ReducedFraction(0,9);

        double exp_res = 0;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void zeroDenomTest() {
        ReducedFraction frac = new ReducedFraction(15,0);

        double exp_res = 0;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }

    @Test
    void zeroTest() {
        ReducedFraction frac = new ReducedFraction(0,0);

        double exp_res = 0;

        double res = frac.toDouble();

        Assert.assertEquals(exp_res, res);
    }
}
