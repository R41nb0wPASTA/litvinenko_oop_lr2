package reducedfractionapp;

import java.util.Objects;

/** Несократимая дробь.
 */
public final class ReducedFraction implements Cloneable {

    public static int gcd(int a, int b) {
        if (b == 0)
            return Math.abs(a);
        return gcd(b, a % b);
    }

    /* =========================== Свойства =============================== */

    /* ---------------------- Числитель и знаменатель --------------------- */
    private int numerator;
    private int denominator;

    public int getNumerator() { return numerator; }
    public int getDenominator() { return denominator; }


    /* =========================== Операции ============================== */

    /* ---------------------------- Порождение ---------------------------- */

    /** Создание дроби с указанием ее числителя и знаменателя.
     *  Если знаменатель = 0, создаётся дробь 0/1.
     *  Если дробь отрицательная, знак минуса всегда переносится в числитель.
     *  @param numerator - числитель
     *  @param denominator - знаменатель
     */
    public ReducedFraction(int numerator, int denominator)
    {
        if(denominator != 0)
        {
            int gcd = gcd(numerator, denominator);

            if(numerator < 0 && denominator < 0 || numerator >= 0 && denominator < 0)
            {
                numerator *= -1;
                denominator *=-1;
            }

            this.numerator = numerator/gcd;
            this.denominator = denominator/gcd;
        }
        else
        {
            this.numerator = 0;
            this.denominator = 1;
        }
    }

    /** Создание дроби на основе целого числа.
     *  @param numerator - числитель
     */
    public ReducedFraction (int numerator)
    {
        this.numerator = numerator;
        this.denominator = 1;
    }

    /* --------------------- Арифметические операции ---------------------- */

    /** Сложение двух дробей.
     *  @param fraction - складываемая дробь
     *  @return новая дробь - результат сложения дробей
     */
    public ReducedFraction sum(ReducedFraction fraction)
    {
        int newNumerator = this.numerator * fraction.getDenominator() +
                fraction.getNumerator() * this.denominator;
        int newDenominator = this.denominator * fraction.getDenominator();

        return new ReducedFraction(newNumerator, newDenominator);
    }

    /** Вычитание двух дробей.
     *  @param fraction - вычитаемая дробь
     *  @return новая дробь - результат вычитания дробей
     */
    public ReducedFraction subtract(ReducedFraction fraction)
    {
        int newNumerator = this.numerator * fraction.getDenominator() -
                fraction.getNumerator() * this.denominator;
        int newDenominator = this.denominator * fraction.getDenominator();

        return new ReducedFraction(newNumerator, newDenominator);
    }

    /** Умножение двух дробей.
     *  @param fraction - умножаемая дробь
     *  @return новая дробь - результат умножения дробей
     */
    public ReducedFraction multiply(ReducedFraction fraction)
    {
        int newNumerator = this.numerator * fraction.getNumerator();
        int newDenominator = this.denominator * fraction.getDenominator();

        return new ReducedFraction(newNumerator, newDenominator);
    }

    /** Деление двух дробей.
     *  @param fraction - дробь для деления
     *  @return новая дробь - результат деления дробей
     */
    public ReducedFraction divide(ReducedFraction fraction)
    {
        int newNumerator = this.numerator * fraction.getDenominator();
        int newDenominator = this.denominator * fraction.getNumerator();

        return new ReducedFraction(newNumerator, newDenominator);
    }

    /* --------------------- Операции сравнения ---------------------- */

    /** Сравнение двух дробей.
     *  @param fraction - сравниваемая дробь
     *  @return целое число: -1 - меньше, 0 - равно, 1 - больше - результат сравнения дробей
     */
    public int compare(ReducedFraction fraction){
        return fraction.denominator != this.denominator?
                Integer.compare(this.numerator * fraction.denominator, fraction.numerator * this.denominator):
                Integer.compare(this.numerator, fraction.numerator);
    }

    /** Эквивалентность двух дробей.
     *  @param other - сравниваемый объект
     *  @return boolean: true - эквиваленты, false - не эквиваленты - результат сравнения эквивалентности дроби
     */
    @Override
    public boolean equals(Object other){
        if(!(other instanceof ReducedFraction))
            return false;

        ReducedFraction fraction = (ReducedFraction)other;
        return fraction.numerator == this.numerator && fraction.denominator == this.denominator;
    }

    /* --------------------- Операции преобразования ---------------------- */

    /** Представить как строку.
     *  @return String - строковое представление дроби
     */
    @Override
    public String toString()
    {
        String str;
        if(this.denominator != 1)
            str = String.format("%d/%d", this.numerator, this.denominator);
        else
            str = String.format("%d", this.numerator);

        return str;
    }

    /** Представить как вещественное число.
     *  @return double - вещественное представление дроби
     */
    public double toDouble()
    {
        return (double)this.numerator/(double)this.denominator;
    }
}
